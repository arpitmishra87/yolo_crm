var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EffortSchema = new Schema({
    effort      : { type: String, lowercase: true, required: true},
    waitage     : { type: Number, required: true},
    remarks     : { type: String, lowercase: true}
    //filename    : { type: String},
    //filedata    : { type: Buffer},
    //mimetype    : { type: String}
});

module.exports = mongoose.model('Efforts',EffortSchema);