var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FollowUpSchema = new Schema({
    client_name         : { type: String, lowercase: true},
    organization_name   : { type: String, lowercase: true},
    client_id           : { type: String, lowercase: true},
    userid              : { type: String, lowercase: true},
    followedby          : { type: String, lowercase: true},
    effort_type         : { type: String, lowercase: true},
    remarks             : { type: String, lowercase: true},
    waitage             : { type: Number},
    date                : { type: Date, default:Date.now},
    followup_date       : { type: Date, default:Date.now},
    followup_reason     : { type: String, lowercase: true},
    status              : { type: String, default: 'cold'}
});

module.exports = mongoose.model('FollowUps', FollowUpSchema);