var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ClientSchema = new Schema({
    client_name         : { type: String, lowercase: true, default:""},
    organization_name   : { type: String, lowercase: true, default:""},
    place               : { type: String, lowercase: true},
    contact_number1     : { type: String, required:true, unique: true},
    contact_number2     : { type: String, default: "" },
    email               : { type: String, default: "" },
    verticals           : { type: String, default: "" },
    start_date          : { type: Date, default: Date.now},
    client_of_user      : { type: [String], default: []},
    isactive            : { type:Boolean, default: true}
    //followups         : { type : Array, default: []}
});

module.exports = mongoose.model('Client', ClientSchema);
