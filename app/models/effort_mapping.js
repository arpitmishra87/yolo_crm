var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EffortMappingSchema = new Schema({
    effortid: { type: String, lowercase: true, required: true},
    userid:{type:String,required:true,lowercase: true}
});

module.exports = mongoose.model('EffortMap',EffortMappingSchema);