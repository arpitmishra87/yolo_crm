var User 		= require('../models/user'),
    Efforts     = require('../models/efforts'),
    Client      = require('../models/clients'),
    FollowUps   = require('../models/followups'),
    jwt 		= require('jsonwebtoken'),
	secret      = 'secret',
	url         = require('url');
//var formidable = require('formidable');

module.exports = function(router){


    // register users
    router.post('/updateuser', function(req,res){

        var id = req.body._id;
        if(!id){
            res.json({success: false, msg :"ID not provided"});
        }
        else {
            var name = req.body.name;
            var username = req.body.username;
            var designation = req.body.designation;
            var email = req.body.email;
            var userid = req.body.userid;
            var contact = req.body.contact;
            var gender = req.body.gender;
            var waitageperday = req.body.waitageperday;
            User.updateOne(
                { _id: id },
                {
                    $set: {
                        name:name,
                        username:username,
                        designation:designation,
                        email:email,
                        userid:userid,
                        contact:contact,
                        gender:gender,
                        perdaywaitage:waitageperday
                    }
                }
                , function(err,data){
                    if(err){
                        res.json({success: false , msg:"Could Not update! Some fields are already exists !", data: data})
                    } else {
                        res.json({success: true, msg : "Updated !", data: data})
                    }
                });
        }
    });

    // login user
    router.post('/login', function(req,res){
        if(!(req.body.username || req.body.userid || req.body.email || req.body.contact ) || !req.body.password){
            res.json({success:false,msg:'Ensure username and password were provided'});
        }
        else{
            var obj = {};
            if(req.body.username){
                obj.username = req.body.username;
            }
            else if(req.body.userid){
                obj.userid = req.body.userid;
            }
            else if(req.body.email){
                obj.email = req.body.email;
            }
            else if(req.body.contact){
                obj.contact = req.body.contact;
            }

            User.findOne( obj ).select('type name username email password userid contact gender juniors isactive mappedefforts perdaywaitage').exec(function(err, user) {
                if(err) throw err;
                if(!user){
                    res.json({success:false, msg:'Could not authenticate user'})
                }else if(user.isactive == false){
                    res.json({success:false, msg:'Could not login, User is deactivated.'})
                }
                else if(user){
                    var validPassword = user.comparePassword(req.body.password);
                    if(!validPassword){
                        res.json({success:false, msg: 'Could not authenticate password'})
                    }
                    else{
                        var token = jwt.sign({
                            name:user.name,
                            username: user.username,
                            email: user.email,
                            userid: user.userid,
                            contact:user.contact,
                            gender:user.gender,
                            type: user.type,
                            juniors:user.juniors,
                            mappedefforts:user.mappedefforts,
                            perdaywaitage:user.perdaywaitage
                        },
                            secret,
                            {
                                expiresIn:'24h'
                            }
                        );
                        res.json({success:true, msg: 'User authenticate!', token: token , data: user});
                    }
                }
            });
        }
    });

    // register admin
    router.post('/registeradmin', function(req,res){
        var user = new User();
        user.name = req.body.name;
        user.username = req.body.username;
        user.designation = req.body.designation;
        user.email = req.body.email;
        user.password = req.body.password;
        user.userid = req.body.userid;
        user.contact = req.body.contact;
        user.gender = req.body.gender;
        user.type = "admin" ;
        var authsecret = "y0l0adm1n";

        if(!req.body.authsecret || !req.body.name || !req.body.username || !req.body.designation || !req.body.email ||  !req.body.password || !req.body.userid || !req.body.contact || !req.body.gender ){
            res.json({success:false,msg:'Ensure all information were provided'});
        }
        else{
            if(authsecret == req.body.authsecret){
                user.save(function(err){
                    if(err) {
                        res.json({success:false,msg:'username or email or userid or contact is already exists !'});
                    }
                    else{
                        res.json({success:true,msg:'admin created !'});
                    }
                });
            } else {
                res.json({success:false,msg:'Could not create admin !'});
            }

        }
    });

    //middleware to ckeck token is valid or not
    router.use(function(req,res,next){
        var token = req.body.token || req.body.query || req.headers['x-access-token'];
        if(token){
            jwt.verify(token, secret, function(err, decoded){
                if(err){
                    res.json({success:false, msg: 'Token Invalid'});
                }
                else {
                    req.decoded = decoded;
                    next();
                }
            });
        }
        else{
            res.json({success:false, msg: 'No Token provided'});
        }
    });

    //get all users
    router.get('/allusers', function(req,res){

        User.aggregate([
            {
                "$lookup": {
                    "from": "followups",
                    "as": "totalWaitage",
                    let: { userID: '$userid' },
                    pipeline: [
                        {
                            $match: {
                                $expr: { $eq: [ '$userid', '$$userID' ] }
                            }
                        },
                        {
                            $group:{
                                "_id": '',
                                total: {
                                    $sum: "$waitage"
                                }
                            }
                        }
                    ]
                }
            }
        ]).exec(function(err, data){
            if(err){ throw err; }
            else {
                res.json({success: true,data: data});
            }
        })


    });

    // get user's details
    router.get('/user', function(req,res){
        var params = url.parse(req.url, true).query
        if(!params.userid){
            res.json({success:false,msg:'userid not provided'});
        } else {
            User.findOne({userid:params.userid}).exec(function(err, data){
                if(err){ throw err; }
                else {
                    res.json({data: data});
                }
            })
        }
    });

    // get users details
    router.get('/users', function(req,res){
        var params = url.parse(req.url, true).query
        if(!params.userid){
            res.json({success:false,msg:'userid not provided'});
        } else {
            var stringIds = params.userid;
            var ids = stringIds.split(',');
            User.find({userid:{"$in":ids}}).exec(function(err, data){
                if(err){ throw err; }
                else {
                    res.json({success: true,data: data});
                }
            })
        }
    });

    // get the information about user from token
    router.post('/me' , function(req,res){
        res.json({success:true, data: req.decoded});
    });

    // added by krishna

    //get all efforts
    router.get('/allefforts', function(req,res){

        Efforts.find({},function(err, data){
            if(err){ throw err; }
            else {
                res.json({success: true, efforts: data});
            }
        });

    });

    // add effort
    router.post('/addeffort', function(req,res){
        var effort = new Efforts();
        effort.effort = req.body.effortname;
        effort.waitage = req.body.effortwaitage;
        effort.remarks = req.body.effortremarks;
        //var policyFile = req.body.policyfile;
        //effort.filename = policyFile.name;
        //effort.filedata = fs.readFileSync(policyFile.name);
        //effort.mimetype = policyFile.type;

        if(!req.body.effortname || !req.body.effortwaitage){
            res.json({success:false,msg:'Ensure all information were provided'});
        }
        else{
            effort.save(function(err){
                if(err) {
                    res.json({success:false,msg:'Effort is already exists !'});
                }
                else{
                    res.json({success:true,msg:'Effort created !'});
                }
            });
        }
    });

    // map effort
    router.post('/mapEffortToUser', function(req,res){
        //var mapEffort = new MapEffort();
        //mapEffort.userid = req.body.userid;
        //mapEffort.effortid = req.body.efforid;

        var userid = req.body.userid;
        var effortidArray = req.body.effortids;

        if(!req.body.userid || !req.body.effortids){
            res.json({success:false,msg:'Ensure all information were provided'});
        }
        else{
            User.updateOne(
                { userid: userid },
                {
                    $set: {
                        mappedefforts:effortidArray
                    }
                }
                , function(err,data){
                    if(err){
                        res.json({success: false , msg:"Could Not update! Some fields are already exists !", data: data})
                    } else {
                        res.json({success: true, msg : "Updated !", data: data})
                    }
                });
        }
    });

    //get all clients
    router.get('/allclients', function(req,res){

        Client.find().exec(function(err, data){
            if(err){ throw err; }
            else {
                res.json({success: true,data: data});
            }
        })
    });

    //get client by id
    router.get('/getClientByID', function(req,res){
        var params = url.parse(req.url, true).query;
        if(!params.id){
            res.json({success:false,msg:'userid not provided'});
        } else {
            Client.find({_id: params.id}).exec(function (err, data) {
                if (err) {
                    console.log(err);
                    throw err;
                }
                else {
                    res.json({success: true, data: data});
                }
            });
        }
    });

    // get all clients for a userid
    router.get('/getMyClients', function(req,res){
        var params = url.parse(req.url, true).query;
        if(!params.userid){
            res.json({success:false,msg:'userid not provided'});
        } else {
            Client.aggregate([
                { $match: { client_of_user: params.userid } },
                {
                    "$project": {
                        "_id": {
                            "$toString": "$_id"
                        },
                        "place":1,
                        "client_name":1,
                        "organization_name":1,
                        "start_date":1,
                        "isactive":1,
                        "client_of_user":1
                    }
                },
                {
                    "$lookup": {
                        "from": "followups",
                        "as": "latest_followup",
                        let: { clientID: '$_id' },
                        pipeline: [
                            {
                                $match: {
                                    $expr: { $eq: [ '$client_id', '$$clientID' ] },
                                    userid: params.userid
                                }
                            },
                            { $sort: { date:-1 } },
                            { $limit:1 }
                        ]
                    }
                }
            ]).exec(function(err, data){
                if(err){ throw err; }
                else {
                    res.json({success: true, clientsForUser: data});
                }
            });
            //Client.aggregate([
            //        {
            //            $match : {
            //                client_of_user : params.userid
            //            }
            //        }
            //    ]
            //).exec(function(err, data){
            //    if(err){ throw err; }
            //    else {
            //        res.json({success: true, clientsForUser: data});
            //    }
            //});
        }
    });

    // get sum of waitage by current date
    router.get('/getTodaysWaitage', function(req,res){
        var params = url.parse(req.url, true).query;
        if(!params.userid){
            res.json({success:false,msg:'userid not provided'});
        } else {
            FollowUps.aggregate([
                {
                    $match : {
                        $and: [
                            {date: {"$gte": new Date( new Date(new Date().toISOString().slice(0,10)).toISOString() ) } },
                            {userid : params.userid}
                        ]
                    }
                },
                {
                    "$group": {
                        "_id": '',
                        total: {
                            $sum: "$waitage"
                        }
                    }
                }
            ]).exec(function(err, data){
                if(err){ throw err; }
                else {
                    res.json({success: true, todaysWaitage: data });
                }
            });
        }
    });

    // add client
    router.post('/addClient', function(req,res){
        var client = new Client();
        client.client_name = req.body.clientname;
        client.organization_name = req.body.orgname;
        client.place = req.body.place;
        client.contact_number1 = req.body.contact_number1;
        client.contact_number2 = req.body.contact_number2;
        client.email = req.body.emailid;
        client.verticals = req.body.verticals;
        client.start_date = req.body.startDate;
        client.isactive = req.body.isactive;
        client.client_of_user = req.body.client_of_user;


        if(!req.body.clientname || !req.body.orgname || !req.body.place || !req.body.startDate){
            res.json({success:false,msg:'Ensure all information were provided'});
        }
        else{
            client.save(function(err){
                if(err) {
                    console.log(err);
                    res.json({success:false,msg:'This Client is already exists !'});
                }
                else{
                    res.json({success:true,msg:'Client created !'});
                }
            });
        }
    });

    // get junior's ids
    router.get('/juniorsid', function(req,res){
        var params = url.parse(req.url, true).query
        if(!params.userid){
            res.json({success:false,msg:'userid not provided'});
        } else {
            User.findOne({userid:params.userid}).exec(function(err, juniorData){
                if(err){ throw err; }
                else if(juniorData){
                    res.json({success: true, juniors: juniorData.juniors});
                }
                else{
                    res.json({success: false, msg: "failed !"});
                }
            })
        }
    });

    router.post('/assignUserToClient', function(req,res){

        var clientId = req.body.clientID;
        var userid = req.body.employeeId;

        if(!req.body.clientID){
            res.json({success:false,msg:'Ensure all information were provided'});
        }
        else{

            Client.updateOne(
                { _id: clientId },
                {
                    $set: {
                        client_of_user:userid
                    }
                }
                , function(err,data){
                    if(err){
                        res.json({success: false , msg:"Could Not update! Some fields are already exists !", data: data})
                    } else {
                        res.json({success: true, msg : "Updated !", data: data})
                    }
                });
        }
    });

    router.post('/updateClient', function(req,res){

        var clientId = req.body.clientID;
        var dataToUpdate = req.body.dataToUpdate;

        if(!req.body.clientID){
            res.json({success:false,msg:'Ensure all information were provided'});
        }
        else{

            Client.updateOne(
                { _id: clientId },
                {
                    $set: {
                        client_name:dataToUpdate.client_name,
                        organization_name:dataToUpdate.org_name,
                        place:dataToUpdate.city+','+dataToUpdate.area+','+dataToUpdate.place,
                        contact_number1:dataToUpdate.contact_number1,
                        contact_number2:dataToUpdate.contact_number2,
                        email:dataToUpdate.email_id,
                        verticals:dataToUpdate.vertical,
                        start_date:dataToUpdate.startDate
                    }
                }
                , function(err,data){
                    if(err){
                        res.json({success: false , msg:"Could Not update! Some fields are already exists !", data: data})
                    } else {
                        res.json({success: true, msg : "Updated !", data: data})
                    }
                });
        }
    });

    // get all followup for a userid
    router.get('/getMyFollowUps', function(req,res){
        var params = url.parse(req.url, true).query;
        if(!params.userid){
            res.json({success:false,msg:'userid not provided'});
        } else {
            console.log(params.getAllFollowUp);
            if(params.getAllFollowUp == 'true'){
                console.log("if me hain");
                FollowUps.find().sort({date:-1}).exec(function(err, data){
                    if(err){ throw err; }
                    else {
                        res.json({success: true, followups: data});
                    }
                });
            }else {
                console.log("else me hain");
                FollowUps.aggregate([
                        {
                            $match: {
                                userid: params.userid
                            }
                        },
                        {$sort: {date: -1}}
                    ]
                ).exec(function (err, data) {
                        if (err) {
                            throw err;
                        }
                        else {
                            res.json({success: true, followups: data});
                        }
                    });
            }
        }
    });

    // get all followup for a userid by date
    router.get('/getFollowUpByDateAndUserid', function(req,res){
        var params = url.parse(req.url, true).query;
        if(!params.userid){
            res.json({success:false,msg:'userid not provided'});
        } else {
            FollowUps.aggregate([
                    {
                        $match : {
                            $and:[
                                {date: {"$gte": new Date( new Date(new Date(params.gteDate).toISOString().slice(0,10)).toISOString() ),"$lte": new Date( new Date(new Date(params.lteDate).toISOString().slice(0,10)).toISOString() ) } },
                                {userid:params.userid}
                            ]
                        }
                    },
                    { $sort: { date: -1 } }
                ]
            ).exec(function(err, data){
                    if(err){ throw err; }
                    else {
                        res.json({success: true, followups: data});
                    }
                });
        }
    });

    // get all followup for a userid
    router.get('/getClientHistory', function(req,res){
        var params = url.parse(req.url, true).query;
        if(!params.userid){
            res.json({success:false,msg:'userid not provided'});
        } else {
            FollowUps.aggregate([
                    {
                        $match : {
                            $and: [
                                {client_id : params.clientid},
                                {userid : params.userid}
                            ]
                        }
                    },
                    { $sort: { date: -1 } }
                ]
            ).exec(function(err, data){
                    if(err){ throw err; }
                    else {
                        res.json({success: true, followups: data});
                    }
                });
        }
    });


    // check if given id is junior of given userid
    router.get('/isjuniorof', function(req,res){
        var params = url.parse(req.url, true).query
        if(!params.userid){
            res.json({success:false,msg:'userid not provided'});
        } else {
            var userid = params.userid;
            var juniorid = params.juniorid;

            User.findOne({userid:userid}).exec(function(err, data){
                if(err){
                    throw err;
                } else if(data){
                    if(data.juniors.find( function(id){ return id == juniorid }) ){
                        res.json({success: true,found: true});
                    }
                    else {
                        res.json({success: true,found: false});
                    }
                }
                else {
                    res.json({success: false,found: false});
                }
            })
        }
    });

    // add client followups
    router.post('/addClientFollowUp', function(req,res){
        var id = req.body.userid;
        var followUps = new FollowUps();
        followUps.client_name = req.body.followupData.clientname;
        followUps.organization_name = req.body.followupData.orgname;
        followUps.client_id = req.body.followupData.clientid;
        followUps.userid = req.body.userid;
        followUps.followedby = req.body.followupData.followedby;
        followUps.effort_type = req.body.followupData.effortname;
        followUps.waitage = req.body.followupData.waitage;
        followUps.remarks = req.body.followupData.remarks;
        followUps.followup_date = req.body.followupData.followup_date;
        followUps.followup_reason = req.body.followupData.followupReason;
        followUps.status = req.body.followupData.status;

        if(!id){
            res.json({success: false, msg :"ID not provided"});
        }
        else {
            followUps.save(function(err){
                if(err) {
                    res.json({success:false,msg:'This Client is already exists !'});
                }
                else{
                    res.json({success:true,msg:'Follow Up saved !'});
                }
            });
            //Client.updateOne(
            //    { _id: id },
            //    {
            //        $set: {
            //            followups:followup
            //        }
            //    },
            //    function(err,data){
            //        if(err){
            //            res.json({success: false , msg:"Could Not update! Some fields are already exists !", data: data})
            //        } else {
            //            res.json({success: true, msg : "Updated !", data: data})
            //        }
            //    });
        }
    });


    //router.post('/uploadFile',function(req, res){
    //    console.log("req start");
    //    console.log(req);
    //    console.log("req end");
    //    var form = new formidable.IncomingForm();
    //    form.parse(req, function (err, fields, files) {
    //        console.log("files obj start");
    //        console.log(files);
    //        console.log("files obj ends");
    //        var oldpath = files.csvreport.path;
    //        console.log(oldpath);
    //        res.write('File uploaded');
    //        res.end();
    //    });
    //});

    // ./added by krishna

    //middleware to check if it's admin or not
    router.use(function(req,res,next){
        var params = url.parse(req.url, true).query;
        var adminid = req.body.adminid || params.adminid;
        if(!adminid){
            res.json({success:false,msg:'Admin id not provided'});
        } else {
            User.findOne( {userid:adminid} ).select('type').exec(function(err, user) {
                if(err) throw err;
                if(!user){
                    res.json({success:false, msg:'Could not authenticate user'});
                }
                else if(user){
                    if(user.type == 'admin'){
                        next();
                    } else {
                        res.json({success:false, msg:"you don't have permission"});
                    }
                }
            });
        }
    });

    // mapping between junior and senior
    router.post('/mapping', function(req,res){
        var seniorid = req.body.seniorid;
        var juniorid = req.body.juniorid;
        //var userid = req.body.userid; // which will perform this operation
        //user who is going to perform this operation must be admin

        if(!req.body.seniorid || !req.body.juniorid){
            res.json({success:false,msg:'Ensure both ids were provided'});
        } else {
            var ids = [];
            ids.push(seniorid);
            ids.push(juniorid);
            User.find({userid:{"$in":ids}}).exec(function(err, user) {
                if(err) throw err;
                else if(user.length == 2){ // if both users are present then only map
                    User.updateOne({userid:seniorid}, {"$push": {"juniors": juniorid}},{ "upsert": true }).exec(function(err, user) {
                        if(err) throw err;
                        else{
                            User.updateOne({userid:juniorid}, {"$push": {"seniors": seniorid}},{ "upsert": true }).exec(function(err, user) {
                                if(err) throw err;
                                else res.json({success:true,msg:'Junior/Senior added!'});
                            })
                        }
                    })
                } else {
                    res.json({success:false,msg:'failed to add Junior/Senior!'});
                }
            });
        }
    });

    // remove mapping between junior and senior
    router.post('/removemapping', function(req,res){
        var seniorid = req.body.seniorid;
        var juniorid = req.body.juniorid;
        //user who is going to perform this operation must be admin

        if(!req.body.seniorid || !req.body.juniorid){
            res.json({success:false,msg:'Ensure both ids were provided'});
        } else {
            var ids = [];
            ids.push(seniorid);
            ids.push(juniorid);
            User.find({userid:{"$in":ids}}).exec(function(err, user) {
                if(err) throw err;
                else if(user.length == 2){ // if both users are present then only map
                    User.updateOne({userid:seniorid}, {"$pull": {"juniors": juniorid}},{ "multi": true }).exec(function(err, user) {
                        if(err) throw err;
                        else{
                            User.updateOne({userid:juniorid}, {"$pull": {"seniors": seniorid}},{ "multi": true }).exec(function(err, user) {
                                if(err) throw err;
                                else res.json({success:true,msg:'Junior/Senior removed!'});
                            })
                        }
                    })
                } else {
                    res.json({success:false,msg:'failed to add Junior/Senior!'});
                }
            });
        }
    });

    // register users
    router.post('/register', function(req,res){
        var user = new User();
        user.name = req.body.name;
        user.username = req.body.username;
        user.designation = req.body.designation;
        user.email = req.body.email;
        user.password = req.body.password;
        user.userid = req.body.userid;
        user.contact = req.body.contact;
        user.gender = req.body.gender;
        user.perdaywaitage = req.body.waitageperday;
        user.type = "user" ;

        if(!req.body.name || !req.body.username || !req.body.email ||  !req.body.password || !req.body.userid || !req.body.contact || !req.body.gender){
            res.json({success:false,msg:'Ensure all information were provided'});
        }
        else{
            user.save(function(err){
                if(err) {
                    res.json({success:false,msg:'username or email or userid or contact is already exists !'});
                }
                else{
                    res.json({success:true,msg:'user created !'});
                }
            });
        }
    });

    // isemployeeactive set new status
    router.post('/isemployeeactive', function(req,res){
        var userid = req.body.userid;
        var status = req.body.status;

        if(!userid){
            res.json({success:false, msg:'user id not provided'});
        } else {
            User.updateOne({userid:userid}, {$set:{"isactive": status }}).exec(function(err, data) {
                if(err) throw err;
                else{
                    res.json({success:true, msg:'Updated !'});
                }
            })
        }
    });

    router.post('/isclientactive', function(req,res){
        var clientid = req.body.clientid;
        var status = req.body.status;

        if(!clientid){
            res.json({success:false, msg:'user id not provided'});
        } else {
            Client.updateOne({_id:clientid}, {$set:{"isactive": status }}).exec(function(err, data) {
                if(err) throw err;
                else{
                    res.json({success:true, msg:'Updated !'});
                }
            })
        }
    });

    return router;
};