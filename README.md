# Read ME

Yolo CRM Management

This will auto assign the client to the user. If Clients Data imported by the CSV using command or by mongodb compass -

After uploading the csv data to clients collection run below code on the clients collection

Make a remote connection to the datebase from terminal:

mongo <host_name>:<port>/<database_name>

This code will split the string "yolo002.yolo003" / [yolo002.yolo003] and update the column value as an array in the database

/***
     if the value of column "client_of_user" is just a string like "yolo002.yolo003", use below code inside the forEach loop
        el.client_of_user = el.client_of_user.split('.');
        db.clients.save(el);
     else
        use this code inside the forEach loop
         el.client_of_user=el.client_of_user.substring(1,el.client_of_user.length-1);
         el.client_of_user = el.client_of_user.split('.');
         db.clients.save(el);
***/

var t = db.clients.find({query for specific user});

t.forEach(function (el) {
    el.client_of_user=el.client_of_user.substring(1,el.client_of_user.length-1);
    el.client_of_user = el.client_of_user.split('.');
    db.clients.save(el);
});