angular.module('appRoutes',['ngRoute'])
.config(function($routeProvider,$locationProvider){
    $routeProvider

        .when('/crm/home', {
            templateUrl:'/crm/public/app/views/pages/home.html'
        })
        .when('/crm/about', {
            templateUrl:'/crm/public/app/views/pages/about.html'
        })
        .when('/crm/register', {
            templateUrl:'/crm/public/app/views/pages/users/register.html',
            controller:'regCtrl'
        })
        .when('/crm/login', {
            templateUrl:'/crm/public/app/views/pages/users/login.html',
            controller:'loginCtrl'
        })
        .when('/crm/logout', {
            templateUrl:'/crm/public/app/views/pages/users/logout.html'
        })
        .when('/crm/profile', {
            templateUrl:'/crm/public/app/views/pages/users/profile.html',
            controller:'profileCtrl'
        })
        .when('/crm/juniors', {
            templateUrl:'/crm/public/app/views/pages/users/juniors.html',
            controller:'juniorsCtrl'
        })
        .when('/crm/junior/:userid', {
            templateUrl:'/crm/public/app/views/pages/users/viewjunior.html',
            controller:'viewJuniorCtrl'
        })
        .when('/crm/junior/profile/:userid', {
            templateUrl:'/crm/public/app/views/pages/users/viewjuniorprofile.html',
            controller:'viewJuniorCtrl'
        })
        .when('/crm/hierarchy', {
            templateUrl:'/crm/public/app/views/pages/admin/hierarchy.html',
            controller:'hierarchyCtrl'
        })
        .when('/crm/employees', {
            templateUrl:'/crm/public/app/views/pages/admin/employees.html',
            controller:'employeesCtrl'
        })
        .when('/crm/addefforts', {
            templateUrl:'/crm/public/app/views/pages/admin/add-effort.html',
            controller:'addeffortCtrl'
        })
        .when('/crm/edituser/:username/:userid', {
            templateUrl:'/crm/public/app/views/pages/admin/edit-user.html',
            controller:'editUserCtrl'
        })
        .when('/crm/editclient/:clientname/:clientid', {
            templateUrl:'/crm/public/app/views/pages/admin/edit-client.html',
            controller:'editClientCtrl'
        })
        .when('/crm/addclient', {
            templateUrl:'/crm/public/app/views/pages/admin/addclient.html',
            controller:'addPartyClientCtrl'
        })
        .when('/crm/work-entry', {
            templateUrl:'/crm/public/app/views/pages/users/workentry.html',
            controller:'workentryCtrl'
        })
        .when('/crm/followups', {
            templateUrl:'/crm/public/app/views/pages/users/followups.html',
            controller:'followupsCtrl'
        })
        //.when('/crm/myclients', {
        //    templateUrl:'/crm/public/app/views/pages/users/myclients.html',
        //    controller:'myclientsCtrl'
        //})
        .when('/crm/export-employee-followups', {
            templateUrl:'/crm/public/app/views/pages/admin/export-employee-followups.html',
            controller:'exportEmpDataCtrl'
        })
        .when('/crm/uploadCsv', {
            templateUrl:'/crm/public/app/views/pages/admin/uploadCsv.html',
            controller:'uploadCsvCtrl'
        })
        .otherwise({ redirectTo: '/crm/login' });

    // to remove # from url of angular
    $locationProvider.html5Mode({
        enabled:true,
        requireBase: false
    });

});