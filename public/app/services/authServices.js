angular.module('authServices',[])
.factory('Auth',function($http,AuthToken){
    var userFactory = {};

    //Auth.registerUser(---)
    userFactory.registerUser = function(name, username, designation ,email, password, userid, contact, gender, waitageperday, adminid,successcallback,errorcallback){
        var data = {
            name:name,
            username:username,
            designation:designation,
            email:email,
            password:password,
            userid:userid,
            contact:contact,
            gender:gender,
            waitageperday:waitageperday,
            adminid:adminid
        };
        return $http({
            url: '/crm/api/register',
            method: "POST",
            data:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    //Auth.updateUserInfo(---)
    userFactory.updateUserInfo = function(name, username,designation, email, userid, contact, gender, waitageperday, adminid, _id ,successcallback, errorcallback){
        var data = {
            name:name,
            username:username,
            designation:designation,
            email:email,
            userid:userid,
            contact:contact,
            gender:gender,
            waitageperday:waitageperday,
            adminid:adminid,
            _id:_id
        };
        return $http({
            url: '/crm/api/updateuser',
            method: "POST",
            data:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    //Auth.login(userData)
    userFactory.login = function(data,successcallback,errorcallback){
        return $http({
            url: '/crm/api/login',
            method: "POST",
            data:data
        }).success(function(data){
            AuthToken.setToken(data.token);
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    //Auth.isLoggedIn()
    userFactory.isLoggedIn = function(){
        if(AuthToken.getToken()){
            return true;
        } else {
            return false;
        }
    };

    //Auth.logout()
    userFactory.logout = function(){
        AuthToken.removeToken();
    };

    //Auth.getUserDetailsUsingIds()
    userFactory.getUserDetailsUsingIds = function(userid,successcallback,errorcallback){
        var data = {
            userid:userid
        };
        return $http({
            url: '/crm/api/users',
            method: "GET",
            params:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    userFactory.isActiveToggle = function(adminid, userid, currentStatus, successcallback, errorcallback ){
        var data = {
            adminid: adminid,
            userid:userid,
            status: !currentStatus  // toggle status
        };
        return $http({
            url: '/crm/api/isemployeeactive',
            method: "POST",
            data:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    //Auth.getAllUsers()
    userFactory.getAllUsers = function(successcallback,errorcallback){
        return $http({
            url: '/crm/api/allusers',
            method: "GET"
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    //Auth.getUserDetailsUsingId()
    userFactory.getUserDetailsUsingId = function(userid,successcallback,errorcallback){

        var data = {
            userid:userid
        };
        return $http({
            url: '/crm/api/user',
            method: "GET",
            params:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    //Auth.getUserFromToken()
    userFactory.getUserFromToken = function(successcallback,errorcallback){
            return $http({
                url: '/crm/api/me',
                method: "POST"
            }).success(function(data){
                successcallback(data);
            }).error(function(data) {
                errorcallback(data);
            });
        };

    // added by krishna

    //Auth.getJuniorsId()
    userFactory.getJuniorsId = function(userid,successcallback,errorcallback){
        var data = {
            userid : userid
        };
        return $http({
            url: '/crm/api/juniorsid',
            method: "GET",
            params:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    userFactory.getAllEfforts = function(successcallback,errorcallback){
        //var data = {
        //    adminid:adminid
        //};
        return $http({
            url: '/crm/api/allefforts',
            method: "GET"
            //params:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    userFactory.addEffort = function(effortData, successcallback, errorcallback){
        var data = {
            effortname:effortData.effortname,
            effortwaitage:effortData.effortwaitage,
            effortremarks:effortData.effortremark,
            //policyfile:effortData.files[0]['_file']
        };
        return $http({
            url: '/crm/api/addeffort',
            method: "POST",
            data:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    userFactory.mapEffortToUser = function(userid, effortids, successcallback, errorcallback){
        var data = {
            userid:userid,
            effortids:effortids
        };
        return $http({
            url: '/crm/api/mapEffortToUser',
            method: "POST",
            data:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };
    //    mapEffortToUser
    userFactory.getMappedEffortsForUser = function(userid, successcallback, errorcallback){
        var data = {
            userid:userid
        };
        return $http({
            url: '/crm/api/getMappedEffortsForUser',
            method: "GET",
            params:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };
    //

    userFactory.isJuniorOf = function(userid,juniorid,successcallback,errorcallback){
        var data = {
            juniorid:juniorid,
            userid : userid
        };
        return $http({
            url: '/crm/api/isjuniorof',
            method: "GET",
            params:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    userFactory.mappingJuniorSenior = function(juniordesignition,seniordesignation,userid,successcallback,errorcallback){
        var data = {
            juniorid:juniordesignition,
            seniorid:seniordesignation,
            adminid:userid
        };
        return $http({
            url: '/crm/api/mapping',
            method: "POST",
            data:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    userFactory.removeMappingJuniorSenior = function(juniorid,seniorid,userid,successcallback,errorcallback){
        var data = {
            juniorid:juniorid,
            seniorid:seniorid,
            adminid:userid
        };
        return $http({
            url: '/crm/api/removemapping',
            method: "POST",
            data:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    //Auth.getAllClients()
    userFactory.getAllClients = function(successcallback,errorcallback){
        return $http({
            url: '/crm/api/allclients',
            method: "GET"
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    userFactory.getClientByID = function(clientid, successcallback, errorcallback){
        var data = {
            id:clientid
        };
        return $http({
            url: '/crm/api/getClientByID',
            method: "GET",
            params:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    //    get clients of user
    userFactory.getMyClients = function(userid, successcallback, errorcallback){
        var data = {
            userid:userid
        };
        return $http({
            url: '/crm/api/getMyClients',
            method: "GET",
            params:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };
    //

    userFactory.assignUserToClient = function(userid, clientid, successcallback, errorcallback){
        var data = {
            employeeId:userid,
            clientID:clientid
        };
        return $http({
            url: '/crm/api/assignUserToClient',
            method: "POST",
            data:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    userFactory.updateClient = function(dataToUpdate, clientid, successcallback, errorcallback){
        var data = {
            dataToUpdate:dataToUpdate,
            clientID:clientid
        };
        return $http({
            url: '/crm/api/updateClient',
            method: "POST",
            data:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    userFactory.addClient = function(clientData, userid, successcallback, errorcallback){
        var data = {
            clientname:clientData.client_name,
            orgname:clientData.org_name,
            emailid:clientData.email_id,
            contact_number1:clientData.contact_number1,
            contact_number2:clientData.contact_number2,
            verticals:clientData.vertical,
            place:clientData.city + "," + clientData.area + "," + clientData.place,
            startDate:clientData.startDate,
            isactive:clientData.isactive,
            client_of_user:userid
        };
        return $http({
            url: '/crm/api/addClient',
            method: "POST",
            data:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    userFactory.toggleClientActiveness = function(adminid, clientid, currentStatus, successcallback, errorcallback ){
        var data = {
            adminid:adminid,
            clientid:clientid,
            status: !currentStatus  // toggle status
        };
        return $http({
            url: '/crm/api/isclientactive',
            method: "POST",
            data:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    // adds client follow up for user
    userFactory.addClientFollowUp = function(followupData, userid, successcallback, errorcallback){
        var data = {
            followupData:followupData,
            userid:userid
        };
        return $http({
            url: '/crm/api/addClientFollowUp',
            method: "POST",
            data:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    //    get user followup
    userFactory.getTodaysWaitage = function(userid, successcallback, errorcallback){
        var data = {
            userid:userid
        };
        return $http({
            url: '/crm/api/getTodaysWaitage',
            method: "GET",
            params:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };
    //

    //    get user followup
    userFactory.getMyFollowUps = function(userid,getAllFollowUp,successcallback, errorcallback){
        var data = {
            userid:userid,
            getAllFollowUp:getAllFollowUp
        };
        return $http({
            url: '/crm/api/getMyFollowUps',
            method: "GET",
            params:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };
    //

    //    get user followup by date
    userFactory.getFollowUpByDateAndUserid = function(data,successcallback, errorcallback){
        var data = {
            userid:data.userid,
            gteDate:data.gteDate,
            lteDate:data.lteDate
        };
        return $http({
            url: '/crm/api/getFollowUpByDateAndUserid',
            method: "GET",
            params:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };
    //

    //    get user followup
    userFactory.getClientHistory = function(clientid, userid,successcallback, errorcallback){
        var data = {
            clientid:clientid,
            userid:userid
        };
        return $http({
            url: '/crm/api/getClientHistory',
            method: "GET",
            params:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };
    //

    // upload files
    userFactory.uploadFile = function(file,successcallback,errorcallback){
        var data = {
            //adminid:adminid,
            files:file
        };
        return $http({
            url: '/crm/api/uploadFile',
            method: "POST",
            data:data
        }).success(function(data){
            successcallback(data);
        }).error(function(data) {
            errorcallback(data);
        });
    };

    return userFactory;
})

.factory('AuthToken',function($window){
    var authTokenFactory = {};

    //AuthToken.setToken(token)
    authTokenFactory.setToken = function(token){
        $window.localStorage.setItem('token',token);
    };

    //AuthToken.getToken()
    authTokenFactory.getToken = function(){
        return $window.localStorage.getItem('token');
    };

    //AuthToken.removeToken()
    authTokenFactory.removeToken = function(){
        $window.localStorage.removeItem('token');
    };

    return authTokenFactory;
})

.factory('AuthInterceptors',function(AuthToken){
    var authInterceptorsFactory = {};
    authInterceptorsFactory.request = function(config){
        var token = AuthToken.getToken();
        if(token) config.headers['x-access-token'] = token;
        return config;
    }
    return authInterceptorsFactory;
})

;