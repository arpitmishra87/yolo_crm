angular.module('userControllers',[])

    .controller('regCtrl', function($scope,$http,$location,$timeout,Auth) {
        if($scope.isLoggedIn && $scope.isAdmin){
            $scope.successMsg = false;
            $scope.errorMsg = false;

            $scope.regUser = function (userData) {
                $scope.successMsg = false;
                $scope.errorMsg = false;
                var successcallbackcreate = function(data){
                    console.log(data);
                    $scope.showNotification(data.msg,data.success);
                    if (data.success) {
                        $scope.successMsg = data.msg;
                    }
                    else {
                        $scope.errorMsg = data.msg;
                    }
                };
                var errorcallback = function(data){
                    $scope.showNotification(data.msg,data.success);
                };
                Auth.registerUser(
                    userData.name,
                    userData.username,
                    userData.designation,
                    userData.email,
                    userData.password,
                    userData.userid,
                    userData.contact,
                    userData.gender,
                    userData.weightageperday,
                    $scope.userdetails.userid,
                    successcallbackcreate,
                    errorcallback);
            }
        } else if($scope.isLoggedIn) {
            $location.path('crm/profile')
        } else {
            $location.path('crm/login')
        }
    })

    .controller('loginCtrl', function($scope,$http,$location,$timeout) {
        if($scope.isLoggedIn){
            $location.path('crm/profile');
        } else {
        }
    })

    .controller('profileCtrl', function($scope,$http,$location,$timeout,Auth) {
        if(!($scope.isLoggedIn)){
            $location.path('crm/login');
        }
        else {

        }
    })

    .controller('juniorsCtrl', function($scope,$http,$location,$timeout,Auth) {

        if(!($scope.isLoggedIn)){
            $location.path('crm/login');
        }
        else {
            $scope.gotJuniors = false;
            $scope.juniorsheander = ['Name', 'Userid', 'Email', 'Contact'];
            $scope.juniorskeys = ['name', 'userid', 'email', 'contact'];

            var errorcallback = function (data) {
                $scope.showNotification(data.msg,data.success);
            };

            var successcallbackjuniorsids = function (data) {
                var i;
                var ids = "";
                for (i = 0; i < data.juniors.length; i++) {
                    var id = data.juniors[i];
                    if (i < data.juniors.length - 1) {
                        ids = ids + id + ",";
                    }
                    else {
                        ids = ids + id;
                    }
                }
                if (ids.length > 0) {
                    var successcallbackjuniors = function (data) {
                        $scope.juniorsData = data.data;
                        $scope.gotJuniors = true;
                    };
                    Auth.getUserDetailsUsingIds(ids, successcallbackjuniors, errorcallback);
                }
            };

            Auth.getJuniorsId($scope.userdetails.userid, successcallbackjuniorsids, errorcallback);

            $scope.viewUser = function (userid) {
                $location.path('crm/junior/' + userid);
            };

            $scope.addJunior = function (juniorid) {
                var successcallbackmap = function (data) {
                    $scope.showNotification(data.msg, data.success);
                    if (data.success) {
                        $scope.juniorid = "";
                        Auth.getJuniorsId($scope.userdetails.userid, successcallbackjuniorsids, errorcallback);
                    }
                };
                Auth.mappingJuniorSenior(juniorid, $scope.userdetails.userid, successcallbackmap, errorcallback);
            };
        }
    })

    .controller('viewJuniorCtrl', function($scope,$http,$location,$timeout,Auth,$routeParams) {

        if(!($scope.isLoggedIn)){
            $location.path('crm/login');
        }
        else {
            $scope.juniorid = $routeParams.userid;

            if($(window).innerWidth() <= 767) {
                $scope.tabularView = false;
            } else {
                $scope.tabularView = true;
            }

            $scope.juniorsheander = ['Name', 'Userid', 'Email', 'Contact'];
            $scope.juniorskeys = ['name', 'userid', 'email', 'contact'];
            $scope.userinfo = {};
            $scope.tab = {};
            $scope.tab.clients = true;
            $scope.tab.profile = false;

            $scope.showTab = function(key){
                $scope.tab.clients = false;
                $scope.tab.profile = false;

                $scope.tab[key] = true;
            };

            $scope.innertab = {};
            $scope.innertab.pending = true;
            $scope.innertab.completed = false;

            $scope.showInnerTab = function(key){
                $('.custom-popover').hide();
                $scope.innertab.pending = false;
                $scope.innertab.completed = false;
                $scope.innertab[key] = true;
            };

            $scope.toggleTaskView = function(view){
                $('.custom-popover').hide();
                if(view == 1)
                    $scope.tabularView = true;
                else
                    $scope.tabularView = false;
            };

            var errorcallback = function (data) {
                $scope.showNotification(data.msg,data.success);
            };

            var successcallbackdetails = function (data) {
                $scope.userinfo.name = data.data.name;
                $scope.userinfo.username = data.data.username;
                $scope.userinfo.email = data.data.email;
                $scope.userinfo.gender = data.data.gender;
                $scope.userinfo.contact = data.data.contact;
                $scope.userinfo.userid = data.data.userid;
            };
            Auth.getUserDetailsUsingId($routeParams.userid, successcallbackdetails, errorcallback);

            $scope.gotTasks = false;
            $scope.tasksheander = ['Name', 'Description', 'Date'];
            $scope.taskskeys = ['name', 'description', 'date'];

            $scope.gotJuniors = false;
            $scope.juniorsheander = ['Name', 'Userid', 'Email', 'Contact'];
            $scope.juniorskeys = ['name', 'userid', 'email', 'contact'];
            var successcallbackjuniorsids = function (data) {
                var i;
                var ids = "";
                for (i = 0; i < data.juniors.length; i++) {
                    var id = data.juniors[i];
                    if (i < data.juniors.length - 1) {
                        ids = ids + id + ",";
                    }
                    else {
                        ids = ids + id;
                    }
                }
                if(ids.length > 0) {
                    var successcallbackjuniors = function (data) {
                        $scope.juniorsData = data.data;
                        if ($scope.juniorsData.length > 0) {
                            $scope.gotJuniors = true;
                        }
                        $scope.gotJuniors = true;
                    };
                    Auth.getUserDetailsUsingIds(ids, successcallbackjuniors, errorcallback);
                }
            };
            Auth.getJuniorsId($routeParams.userid, successcallbackjuniorsids, errorcallback);

            var successcallbackGetMyClient = function(data){
                console.log(data);
                $scope.clientsOfUser = data.clientsForUser;
                $scope.gotClients = true;
            };

            function getMyClients(){
                Auth.getMyClients($scope.juniorid, successcallbackGetMyClient, errorcallback);
            }

            getMyClients();

            var successcallbackclientHistory = function (data) {
                console.log(data);
                $scope.historyData = data.followups;
            };

            $scope.showPopOver = function(event, clientID) {
                var pos = $('#'+event.target.id).offset();
                $('.custom-popover').css('top', pos.top-46 );
                Auth.getClientHistory(clientID, $scope.juniorid, successcallbackclientHistory, errorcallback);
                $('.custom-popover').toggle();
            };
        }
    })

    .controller('workentryCtrl', function($scope,$http,$location,$timeout,Auth,$routeParams,$timeout) {
        if(!($scope.isLoggedIn)){
            $location.path('crm/login');
        }
        else {
            $scope.data = {};
            $scope.perdaywaitage = $scope.userdetails.perdaywaitage;
            $scope.chosenClient = "";

            $(function () {
                var date = new Date();
                var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
                var end = new Date(date.getFullYear(), date.getMonth(), date.getDate()+5);

                $(".followupDate").datepicker({
                    format: 'dd/mm/yyyy',
                    todayHighlight: true,
                    //startDate:today,
                    autoclose: true
                }).on('changeDate', function(ev) {
                    console.log(ev);
                    $('.dateoffollowUp').val(ev.date.toISOString());
                });
                $(".followupDate").datepicker('setDate', 'now');

                $('.slideDownBtn').click(function(){
                    $(this).children().toggleClass('fa fa-arrow-down fa fa-arrow-up');
                    $('.follow-up-form').slideToggle(500);
                });

                $(window).click(function() {
                    //Hide the menus if visible
                    $(".select-search-box").removeClass("show-select-list");
                });

                $(window).keydown(function(event){
                    if(event.keyCode == 13) {
                        event.preventDefault();
                        return false;
                    }
                });

                $("#myClientsList").click(function(event){
                    event.stopPropagation();
                    $(".select-search-box").addClass("show-select-list");
                });

                //$('#myClientsList').selectize({
                //    searchField: ['text']
                //});

            });

            $scope.selectedClient = function(selectedClient){
                $scope.data.selectedClient = selectedClient;
                $scope.chosenClient = selectedClient.client_name;
                $(".select-search-box").removeClass("show-select-list");
            };

            // filters tasks on tabular view for both pending and completed tabs
            $scope.filterList = function(event,listIDName){
                // Declare variables
                var input, filter, list, li, container, i, txtValue;
                input = document.getElementById(event.target.id);
                filter = input.value.toUpperCase();
                list = document.getElementById(listIDName);
                container = $('#customSearchSelectBox');
                li = list.getElementsByTagName("li");
                if (["ArrowDown", "ArrowUp", "Enter"].indexOf(event.key) != -1) {
                    keyControl(event, container)
                }else {
                    // Loop through all table rows, and hide those who don't match the search query
                    for (i = 0; i < li.length; i++) {
                        //td = tr[i].getElementsByTagName("td")[index];
                        if (li) {
                            txtValue = li[i].innerText;
                            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                                li[i].style.display = "";
                            } else {
                                li[i].style.display = "none";
                            }
                        }
                    }
                }

                function keyControl(e, container) {
                    if (e.key == "ArrowDown") {

                        if (container.find("ul li").hasClass("search-selected")) {
                            if (container.find("ul li:visible").index(container.find("ul li.search-selected")) + 1 < container.find("ul li:visible").length) {
                                container.find("ul li.search-selected").removeClass("search-selected").nextAll().not('[style*="display: none"]').first().addClass("search-selected");
                            }

                        } else {
                            container.find("ul li:first-child").addClass("search-selected");
                        }

                    } else if (e.key == "ArrowUp") {

                        if (container.find("ul li:visible").index(container.find("ul li.search-selected")) > 0) {
                            container.find("ul li.search-selected").removeClass("search-selected").prevAll().not('[style*="display: none"]').first().addClass("search-selected");
                        }
                    } else if (e.key == "Enter") {
                        container.find("input").val(container.find("ul li.search-selected").text()).blur();
                        //onSelect(container.find("li.search-selected").attr("data-value"))
                        //container.find("ul li.search-selected").trigger('click');
                        $timeout(function() {
                            //angular.element(domElement).triggerHandler('click');
                            container.find("ul li.search-selected").trigger('click');
                        }, 0);
                    }

                    container.find("ul li.search-selected")[0].scrollIntoView({
                        behavior: "smooth",
                    });
                }
            };
            //


            var errorcallback = function(data){
                $scope.showNotification(data.msg,data.success);
                $scope.nodata = true;
            };

            var successcallbackGetMyClient = function(data){
                console.log(data);
                $scope.clientsOfUser = data.clientsForUser;
                $scope.gotClients = true;
            };

            function getMyClients(){
                Auth.getMyClients($scope.userdetails.userid, successcallbackGetMyClient, errorcallback);
            }

            getMyClients();

            var successcallbackallefforts = function(data){
                console.log(data);
                $scope.effortList = data.efforts;
                getmappedAndUnmappedEfforts();
            };

            Auth.getAllEfforts(successcallbackallefforts, errorcallback);

            var successcallbackForWaitageCalc = function(data){
                console.log(data);
                $scope.todaysWaitage = data.todaysWaitage[0].total;
            };
            function getTodayWaitage() {
                Auth.getTodaysWaitage($scope.userdetails.userid, successcallbackForWaitageCalc, errorcallback);
            }

            getTodayWaitage();

            function getmappedAndUnmappedEfforts(){
                $scope.mappedeffortList = [];
                var effortObj = {};
                for(var i = 0; i < $scope.effortList.length; i++){
                    for(var j = 0; j < $scope.userdetails.mappedefforts.length; j++){
                        if($scope.userdetails.mappedefforts[j]['effortid'] == $scope.effortList[i]._id){
                            effortObj = {
                                _id:$scope.effortList[i]._id,
                                effort:$scope.effortList[i].effort,
                                waitage:$scope.userdetails.mappedefforts[j].waitage,
                                isMapped:true
                            };
                            $scope.mappedeffortList.push(effortObj);
                        }
                    }
                    $scope.gotMappedEfforts = true;
                }
                console.log($scope.mappedeffortList);
            }

            $scope.fileNameChanged = function(element) {
                $scope.fileNames = [];
                $scope.$apply(function(scope) {
                    console.log('files:', element.files);
                    // Turn the FileList object into an Array

                    for (var i = 0; i < element.files.length; i++) {
                        $scope.data.files.push({_file:element.files[i]});
                        $scope.fileNames.push(element.files[i].name)
                    }
                    $scope.data.files.splice(0,1);
                    console.log($scope.data.files)
                });
            };

            $scope.addClientFollowUpDetails = function(data){
                console.log(data);
                var followupData = {};
                followupData.clientid = data.selectedClient._id;
                followupData.clientname = data.selectedClient.client_name;
                followupData.orgname = data.selectedClient.organization_name;
                followupData.effortname = data.selectedEffort.effort;
                followupData.waitage = data.selectedEffort.waitage;
                followupData.remarks = data.remark;
                followupData.followedby = $scope.userdetails.username;
                followupData.followup_date = $('.dateoffollowUp').val();
                followupData.followupReason = data.followup_reason;
                followupData.status = data.status;

                var successcallbackaddClientFollowUp = function(data){
                    $scope.showNotification(data.msg, data.success);
                    if (data.success) {
                        $('.effort-form').each(function () {
                            this.reset();
                        });
                        getMyClients();
                        getTodayWaitage()
                    }
                };
                console.log(followupData);
                Auth.addClientFollowUp(followupData, $scope.userdetails.userid, successcallbackaddClientFollowUp, errorcallback);
            };
        }
    })

    .controller('followupsCtrl', function($scope,$http,$location,$timeout,Auth,$routeParams) {
        if(!($scope.isLoggedIn)){
            $location.path('crm/login');
        }
        else {

            var errorcallback = function(data){
                $scope.showNotification(data.msg,data.success);
            };

            var successcallbackGetMyClient = function(data){
                console.log(data);
                $scope.followups = data.followups;
            };

            function getMyFollowUps(){
                var getAllFollowUp = false;
                if($scope.isAdmin)
                    getAllFollowUp = true;
                Auth.getMyFollowUps($scope.userdetails.userid, getAllFollowUp,successcallbackGetMyClient, errorcallback);
            }

            getMyFollowUps();

        }
    })

    .controller('myclientsCtrl', function($scope,$http,$location,$timeout,Auth,$routeParams) {
        if(!($scope.isLoggedIn)){
            $location.path('crm/login');
        }
        else {

            var errorcallback = function(data){
                $scope.showNotification(data.msg,data.success);
                $scope.nodata = true;
            };

            var successcallbackGetMyClient = function(data){
                console.log(data);
                $scope.today = new Date();
                $scope.clientsOfUser = data.clientsForUser;
                $scope.gotClients = true;
            };

            function getMyClients(){
                Auth.getMyClients($scope.userdetails.userid, successcallbackGetMyClient, errorcallback);
            }

            getMyClients();

            var successcallbackclientHistory = function (data) {
                console.log(data);
                $scope.historyData = data.followups;
                $scope.lastUpdatedFollowUp = data.followups[0];
            };

            $scope.showPopOver = function(event, clientID) {
                var pos = $('#'+event.target.id).offset();
                $('.custom-popover').css('top', pos.top-46 );
                Auth.getClientHistory(clientID, $scope.userdetails.userid, successcallbackclientHistory, errorcallback);
                $('.custom-popover').toggle();
            };

        }
    })

;