angular.module('adminController',[])

.controller('hierarchyCtrl', function($scope,Auth,$timeout){
    var errorcallback = function(data){
        $scope.showNotification(data.msg,data.success);
    };
    var dataIndex = {};
    var dataArray = "";

    var successcallbackallusers = function(data){
        dataArray = data.data;
        dataIndex = {};

        for(var i=0;i<data.data.length;i++){
            dataIndex[ data.data[i].userid ] = i;
        }

        var isJuniorExist = function(parentIndex, juniorIndex){
            if(data.data[parentIndex].juniors[juniorIndex] in dataIndex){
                if(dataArray[dataIndex[data.data[i].juniors[j]]].isactive)
                    return true;
            }
            return false;
        };

        for(var i=0;i<data.data.length;i++){
            if(data.data[i].seniors.length == 0 && data.data[i].isactive){
                $scope.drawChart(data.data[i].userid,'',dataIndex[data.data[i].userid],null);
            }
            for(var j=0;j<data.data[i].juniors.length;j++){
                if(data.data[i].isactive && isJuniorExist(i, j) )
                    $scope.drawChart(data.data[i].juniors[j],data.data[i].userid, dataIndex[data.data[i].juniors[j]],dataIndex[data.data[i].userid]);
            }
        }
    };
    google.charts.setOnLoadCallback(loadChart);
    var data = "";
    var chart = "";
    function loadChart(){
        data = new google.visualization.DataTable();
        chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
        data.addColumn('string', 'Name');
        data.addColumn('string', 'Manager');
        Auth.getAllUsers(successcallbackallusers,errorcallback);
    }
    $scope.drawChart = function(chieldId,parentId,chieldInfoIndex,parentInfoIndex){
        var chieldName = 'c' ;
        var parentName = 'p' ;
        var chlddesignation = '';
        var parentdesignation = '';
        if(chieldInfoIndex != null){
            chieldName = dataArray[chieldInfoIndex].name;
            chlddesignation = dataArray[chieldInfoIndex].designation;
        }
        if(parentInfoIndex != null){
            parentName = dataArray[parentInfoIndex].name;
            parentdesignation = dataArray[parentInfoIndex].designation;
        }
        var chieldCode = chieldId + '<div style="font-size:15px;font-weight: 900;">'+ chieldName + '</div>'+'<div style="font-size:10px;font-weight: 700;">'+ chlddesignation + '</div>';
        var parentCode = parentId + '<div style="font-size:15px;font-weight: 900;">'+ parentName + '</div>'+'<div style="font-size:10px;font-weight: 700;">'+ parentdesignation + '</div>';

        data.addRows([
            [
                {v:chieldId, f: chieldCode},
                {v:parentId, f:parentCode}
            ]
        ]);
        chart.draw(data, {allowHtml:true});
    };
    $scope.addJuniorSenior = function(juniorID,seniorID){
        var successcallbackmapping = function(data){
            if(data.success){
                $scope.parentId = "";
                $scope.chieldId = "";
                loadChart();
            }
            $scope.showNotification(data.msg,data.success);
        };
        Auth.mappingJuniorSenior(juniorID,seniorID,$scope.userdetails.userid,successcallbackmapping,errorcallback);
    };
    $scope.removeJuniorSenior = function(juniorID,seniorID){
        var successcallbackmappingremove = function(data){
            if(data.success){
                $scope.parentId = "";
                $scope.chieldId = "";
                loadChart();
            }
            $scope.showNotification(data.msg,data.success);
        };
        Auth.removeMappingJuniorSenior(juniorID,seniorID,$scope.userdetails.userid,successcallbackmappingremove,errorcallback);
    };
})

.controller('employeesCtrl', function($scope,Auth,$location){
    if($scope.isAdmin && $scope.isLoggedIn){

        var errorcallback = function(){
            $scope.showNotification(data.msg,data.success);
        };
        $scope.editButton = "EDIT";
        $scope.gotJuniors = false;
        $scope.juniorsheander = ['Name', 'Userid', 'Email', 'Contact'];
        $scope.juniorskeys = ['name', 'userid', 'email', 'contact'];

        $scope.tab = {};
        $scope.tab.active = true;
        $scope.tab.deactive = false;

        $scope.showTab = function(key){
            $scope.tab.active = false;
            $scope.tab.deactive = false;
            $scope.tab[key] = true;
        };

        var successcallbackallusers = function(data){
            $scope.juniorsData = data.data;
            $scope.gotJuniors = true;
        };
        Auth.getAllUsers(successcallbackallusers,errorcallback);

        $scope.toggleActive = function(currentstatus,userid, hasJuniors){
            var successcallbackisactive = function(data){
                $scope.showNotification(data.msg,data.success);
                if(data.success){
                    Auth.getAllUsers(successcallbackallusers,errorcallback);
                }
            };
            if(hasJuniors == 0)
                Auth.isActiveToggle($scope.userdetails.userid, userid, currentstatus,successcallbackisactive,errorcallback);
            else
                $scope.showNotification('User has juniors. Please update mapping before deactivating.', false);
        };

    } else if($scope.isLoggedIn) {
        $location.path('crm/profile')
    } else {
        $location.path('crm/login')
    }
})

.controller('addPartyClientCtrl', function($scope,$http,$location,$timeout,Auth,$routeParams) {
        console.log('on addPartyClient Ctrl');
        if(!($scope.isLoggedIn)){
            $location.path('crm/login');
        }
        else {
            $scope.data = {};
            //$scope.data.files = [{_file:null}];

            $(function () {
                var date = new Date();
                var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
                var end = new Date(date.getFullYear(), date.getMonth(), date.getDate()+5);

                $(".startDate").datepicker({
                    format: 'dd/mm/yyyy',
                    todayHighlight: true,
                    //startDate:today,
                    autoclose: true
                }).on('changeDate', function(ev) {
                    console.log(ev);
                    $('.dateofstart').val(ev.date);
                });
                $(".startDate").datepicker('setDate', 'now');

            });

            var errorcallback = function(data){
                $scope.showNotification(data.msg,data.success);
                //$scope.nodata = true;
            };

            var successcallbackGetAllClient = function(data){
                console.log(data);
                $scope.allClients = data.data;
                $scope.gotClients = true;
            };

            var successcallbackGetMyClient = function(data){
                console.log(data);
                $scope.today = new Date();
                $scope.clientsOfUser = data.clientsForUser;
                $scope.gotClients = true;
            };

            function getClients(){
                if($scope.isAdmin)
                    Auth.getAllClients(successcallbackGetAllClient, errorcallback);
                else
                    Auth.getMyClients($scope.userdetails.userid, successcallbackGetMyClient, errorcallback);
            }

            getClients();

            $scope.addClient = function(data){
                data.startDate = $('.dateofstart').val();
                var userid = [];
                if($scope.isAdmin) {
                    data.isactive = true;
                }else {
                    data.isactive = false;
                    userid.push($scope.userdetails.userid);
                }
                console.log(data);
                var successcallbackaddClient = function(data2){
                    $scope.showNotification(data2.msg, data2.success);
                    if (data2.success) {
                        $('.effort-form').each(function () {
                            this.reset();
                        });
                        getClients();
                    }
                };

                Auth.addClient(data, userid, successcallbackaddClient, errorcallback);
            };

            $scope.toggleClientActiveness = function(currentstatus,clientid){
                var successcallbackisactive = function(data){
                    $scope.showNotification(data.msg,data.success);
                    if(data.success){
                        getClients();
                    }
                };
                Auth.toggleClientActiveness($scope.userdetails.userid, clientid, currentstatus, successcallbackisactive, errorcallback);
            }

            var successcallbackclientHistory = function (data) {
                console.log(data);
                $scope.historyData = data.followups;
                $scope.lastUpdatedFollowUp = data.followups[0];
            };

            $scope.showPopOver = function(event, clientID) {
                var pos = $('#'+event.target.id).offset();
                $('.custom-popover').css('top', pos.top-46 );
                Auth.getClientHistory(clientID, $scope.userdetails.userid, successcallbackclientHistory, errorcallback);
                $('.custom-popover').toggle();
            };

            $scope.removeFromMyClient = function(clientsOfUser, clientID){

                if(clientsOfUser.indexOf($scope.userdetails.userid) != -1) {
                    clientsOfUser.splice(clientsOfUser.indexOf($scope.employeesUserId), 1);
                }
                var successcallbackOfClientRemover = function(data){
                    console.log(data);
                    $scope.showNotification(data.msg,data.success);
                    if (data.success) {
                        $scope.successMsg = data.msg;
                        getClients();
                    }
                    else {
                        console.log(data.msg);
                        $scope.errorMsg = data.msg;
                    }
                };

                Auth.assignUserToClient(clientsOfUser, clientID, successcallbackOfClientRemover, errorcallback);
            };

        }
    })

.controller('editClientCtrl', function($scope,Auth,$location,$routeParams){
        if(!($scope.isLoggedIn)){
            $location.path('crm/login');
        }
        else {
            $scope.data = {};
            $scope.clientName = $routeParams.clientname;
            $scope.clientid = $routeParams.clientid;
            console.log($scope.clientid);

            $(function () {
                var date = new Date();
                var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
                var end = new Date(date.getFullYear(), date.getMonth(), date.getDate()+5);

                $(".startDate").datepicker({
                    format: 'dd/mm/yyyy',
                    todayHighlight: true,
                    //startDate:today,
                    autoclose: true
                }).on('changeDate', function(ev) {
                    console.log(ev);
                    $('.dateofstart').val(ev.date);
                });
                //$(".startDate").datepicker('setDate', 'now');

            });

            var errorcallback = function(data){
                $scope.showNotification(data.msg,data.success);
                //$scope.nodata = true;
            };

            var successcallbackGetClient = function(data){
                console.log(data);
                var clientData = data.data[0];
                $scope.data['client_name'] = clientData.client_name;
                $scope.data['org_name'] = (clientData.organization_name) ? clientData.organization_name : clientData.client_name;
                $scope.data['email_id'] = (clientData.email == '--') ? '' : clientData.email;
                $scope.data['contact_number1'] = clientData.contact_number1;
                $scope.data['contact_number2'] = (clientData.contact_number2 == '--') ? '' : clientData.contact_number2;
                $scope.data['vertical'] = (clientData.verticals == '--') ? '' : clientData.verticals;
                var place = clientData.place.split(',');
                $scope.data['city'] = place[0];
                $scope.data['area'] = place[1];
                $scope.data['place'] = place[2];
                var date = new Date(clientData.start_date)
                $scope.data['startDate'] = date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear();
                //var mainDa = date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear();
                $(".startDate").datepicker('setDate', $scope.data['startDate']);
                $scope.gotClients = true;
            };

            function getClientById(){
                Auth.getClientByID($scope.clientid, successcallbackGetClient, errorcallback);
            }

            getClientById();

            $scope.updateClientInfo = function(data){
                //delete data.startDate;
                data.startDate = $('.dateofstart').val();
                console.log(data);
                var successcallbackupdateClient = function(data2){
                    $scope.showNotification(data2.msg, data2.success);
                    if (data2.success) {
                        getClientById();
                    }
                };

                Auth.updateClient(data, $scope.clientid, successcallbackupdateClient, errorcallback);
            };

        }
})

.controller('addeffortCtrl', function($scope,Auth,$location){
    if($scope.isAdmin && $scope.isLoggedIn){

        console.log('on addeffort ctrl');
        $scope.data = {};
        $scope.data.files = [{_file:null}];

        var errorcallback = function(data){
            $scope.showNotification(data.msg,data.success);
            $scope.nodata = true;
        };

        var successcallbackallefforts = function(data){
            console.log(data);
            $scope.effortList = data.efforts;
            $scope.gotEfforts = true;
        };

        function loadAllEfforts() {
            Auth.getAllEfforts(successcallbackallefforts, errorcallback);
        }

        $scope.fileNameChanged = function(element) {
            $scope.fileNames = [];
            $scope.$apply(function(scope) {
                console.log('files:', element.files);
                // Turn the FileList object into an Array

                for (var i = 0; i < element.files.length; i++) {
                    $scope.data.files.push({_file:element.files[i]})
                    $scope.fileNames.push(element.files[i].name)
                }
                $scope.data.files.splice(0,1);
                console.log($scope.data.files)
            });

            var successcallbackuploadfile = function(data){
                console.log(data);
            };
            console.log($scope.data.files[0]['_file']);
            //Auth.uploadFile($scope.data.files, successcallbackuploadfile, errorcallback);
        };

        $scope.addEffort = function(effortData){
            console.log(effortData);
            $scope.successMsg = false;
            $scope.errorMsg = false;
            var successcallbackcreate = function(data){
                console.log(data);
                $scope.showNotification(data.msg,data.success);
                if (data.success) {
                    $scope.successMsg = data.msg;
                    $( '.effort-form' ).each(function(){
                        this.reset();
                    });
                    loadAllEfforts();
                }
                else {
                    $scope.errorMsg = data.msg;
                }
            };
            Auth.addEffort(effortData, successcallbackcreate, errorcallback);
        };

        loadAllEfforts()

    } else {
        $location.path('crm/login')
    }
})

.controller('uploadCsvCtrl', function($scope,Auth,$location,$routeParams){
    if($scope.isAdmin && $scope.isLoggedIn) {
        console.log('on addeffort ctrl');
        $scope.data = {};
        $scope.data.files = [{_file:null}];

        var errorcallback = function(data){
            $scope.showNotification(data.msg,data.success);
            $scope.nodata = true;
        };

        $scope.fileNameChanged = function(element) {
            $scope.fileNames = [];
            $scope.$apply(function(scope) {
                console.log('files:', element.files);
                // Turn the FileList object into an Array
                for (var i = 0; i < element.files.length; i++) {
                    $scope.data.files.push({_file:element.files[i]});
                    $scope.fileNames.push(element.files[i].name)
                }
                $scope.data.files.splice(0,1);
                console.log($scope.data.files)
            });


            console.log($scope.data.files[0]['_file']);
        };

        $scope.uploadCSVToDB = function(effortData) {
            console.log(effortData);
            var successcallbackuploadfile = function(data){
                console.log(data);
            };
            Auth.uploadFile($scope.data.files[0]['_file'], successcallbackuploadfile, errorcallback);

        };

    }else{
        $location.path('crm/login')
    }
})

.controller('editUserCtrl', function($scope,Auth,$location,$routeParams){
    if($scope.isAdmin && $scope.isLoggedIn) {

        $scope.employeeUsername = $routeParams.username;
        $scope.employeesUserId = $routeParams.userid;
        $scope.tab = $scope.data = {};
        $scope.tab.profile = true;
        $scope.tab.mapeffort = false;
        $scope.tab.maptask = false;
        $scope.updateForm = true;
        var _id = "";
        var mappedEfforts = [];
        var effortValue = 0;
        var effortObj = {};
        $scope.showTab = function (key) {
            $scope.tab.profile = false;
            $scope.tab.mapeffort = false;
            $scope.tab.maptask = false;
            $scope.tab[key] = true;
        };

        var errorcallback = function (data) {
            $scope.showNotification(data.msg,data.success);
            $scope.nodata = true;
        };

        // edit employee's profile

        var successcallbackdetails = function (data) {
            $scope.data.name = data.data.name;
            $scope.data.username = data.data.username;
            $scope.data.designation = data.data.designation;
            $scope.data.gender = data.data.gender;
            $scope.data.weightageperday = data.data.perdaywaitage;
            $scope.data.userid = data.data.userid;
            $scope.data.password = data.data.password;
            $scope.data.email = data.data.email;
            $scope.data.contact = parseInt(data.data.contact);
            _id = data.data._id;
            mappedEfforts = data.data.mappedefforts;
            console.log(mappedEfforts);
        };

        function getUserById(){
            Auth.getUserDetailsUsingId($routeParams.userid, successcallbackdetails, errorcallback);
        }

        getUserById();

        $scope.updateUser = function(data){
            var successcallbackupdateuser = function(data){
                $scope.showNotification(data.msg, data.success);
                if(data.success){
                    getUserById();
                }
            };
            Auth.updateUserInfo(
                data.name,
                data.username,
                data.designation,
                data.email,
                data.userid,
                data.contact,
                data.gender,
                data.weightageperday,
                $scope.userdetails.userid,
                _id,
                successcallbackupdateuser,
                errorcallback
            );
        };

        // ./edit employee's profile

        // map effort for employee

        var successcallbackallefforts = function(data){
            //console.log(data);
            $scope.effortList = data.efforts;
            $scope.gotEfforts = true;
            getmappedAndUnmappedEfforts();
        };

        Auth.getAllEfforts(successcallbackallefforts, errorcallback);

        function getmappedAndUnmappedEfforts(){

            $scope.mappedeffortList = [];
            var effortObjList = {};
            for(var i = 0; i < $scope.effortList.length; i++){
                var gotIt = false;
                for(var j = 0; j < mappedEfforts.length; j++){
                    if(mappedEfforts[j]['effortid'] == $scope.effortList[i]._id){
                        effortObjList = {
                            _id:$scope.effortList[i]._id,
                            effort:$scope.effortList[i].effort,
                            waitage:mappedEfforts[j].waitage,
                            isMapped:true
                        };
                        gotIt = true;
                    }
                }
                if(!gotIt){
                    effortObjList = {
                        _id:$scope.effortList[i]._id,
                        effort:$scope.effortList[i].effort,
                        waitage:$scope.effortList[i].waitage,
                        isMapped:false
                    };
                }
                $scope.mappedeffortList.push(effortObjList);
                $scope.gotMappedEfforts = true;
            }
            console.log($scope.mappedeffortList);
        }

        $scope.saveEffortCount = function(event, effortId, effortName){
            if (event.which === 13) {
                console.log(event);
                console.log(effortId);
                effortValue = event.target.value;
                console.log(effortValue);
                effortObj['effortid'] = effortId;
                effortObj['name'] = effortName;
                effortObj['waitage'] = effortValue;
                console.log(effortObj);
                $scope.showNotification("Weightage has updated for user!!!",true);
            }
        };

        $scope.mapEffortForUser = function(userIdToMap,effortId, effortName, effortWaitage){
            console.log(effortObj);
            var removed = false;
            for(var j = 0; j < mappedEfforts.length; j++){
                if(mappedEfforts[j]['effortid'] == effortId){
                    mappedEfforts.splice(j,1);
                    removed = true;
                    break;
                }
            }
            if(!removed){
                if(effortObj.effortid != undefined){
                    mappedEfforts.push(effortObj);
                }
                else {
                    var obj = {
                        effortid: effortId,
                        name: effortName,
                        waitage: effortWaitage
                    };
                    mappedEfforts.push(obj);
                }
            }
            $scope.successMsg = false;
            $scope.errorMsg = false;
            var errorcallback = function(err){
                console.log(err);
            };

            var successcallbackallefforts = function(data){
                //console.log(data);
                $scope.showNotification(data.msg,data.success);
                if (data.success) {
                    $scope.successMsg = data.msg;
                    getmappedAndUnmappedEfforts();
                    effortObj = {};
                }
                else {
                    console.log(data.msg);
                    $scope.errorMsg = data.msg;
                }
            };

            Auth.mapEffortToUser(userIdToMap, mappedEfforts, successcallbackallefforts, errorcallback);
        };

        // ./map effort for employee
        $scope.userWhoCanHandleClient = {};
        // map task & comment assigners
        var successcallbackallusers = function(data){
            console.log(data.data);
            $scope.allClientsData = [];
            data.data.forEach(function(row){
                $scope.allClientsData.push({
                    clientname:row.client_name,
                    orgname:row.organization_name,
                    isactive:row.isactive,
                    canHandleClient:(row.client_of_user.indexOf($scope.employeesUserId) != -1) ? true : false,
                    _id:row._id
                });
                $scope.userWhoCanHandleClient[row._id] = row.client_of_user;
            });
            console.log($scope.userWhoCanHandleClient);
            $scope.gotAllUsers = true;
        };
        function fetchAllClients() {
            Auth.getAllClients(successcallbackallusers, errorcallback);
        }
        fetchAllClients();
        $scope.toggleClientAssigner = function(employeeUserId, clientID){
            var employeeIdWhomClientAssign = $scope.userWhoCanHandleClient[clientID];
            console.log(employeeIdWhomClientAssign);

            if(employeeIdWhomClientAssign.indexOf($scope.employeesUserId) == -1)
                employeeIdWhomClientAssign.push($scope.employeesUserId); // assigns task assigner
            else // removes task assigner
                employeeIdWhomClientAssign.splice(employeeIdWhomClientAssign.indexOf($scope.employeesUserId),1);
            var successcallbackOfClientAssigner = function(data){
                console.log(data);
                $scope.showNotification(data.msg,data.success);
                if (data.success) {
                    $scope.successMsg = data.msg;
                    fetchAllClients();
                }
                else {
                    console.log(data.msg);
                    $scope.errorMsg = data.msg;
                }
            };

            Auth.assignUserToClient(employeeIdWhomClientAssign, clientID, successcallbackOfClientAssigner, errorcallback);
        };

        // ./ map task & comment assigners

    }else{
        $location.path('crm/login')
    }
})

.controller('exportEmpDataCtrl', function($scope,Auth,$location,$routeParams){
    console.log("exportEmpData Ctrl");
    if($scope.isAdmin && $scope.isLoggedIn) {
        $scope.data = {};
        //$scope.data.files = [{_file:null}];

        $(function () {
            var date = new Date();
            var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
            var end = new Date(date.getFullYear(), date.getMonth(), date.getDate()+5);

            $(".followUpOfDate").datepicker({
                format: 'yyyy-mm-dd',
                todayHighlight: true,
                //startDate:today,
                autoclose: true
            }).on('changeDate', function(ev) {
                console.log(ev);
                $('.dateofstart').val(ev.date);
            });
            $(".followUpOfDate").datepicker('setDate', 'now');

        });

        var errorcallback = function(data){
            $scope.showNotification(data.msg,data.success);
            //$scope.nodata = true;
        };

        var successcallbackallusers = function(data){
            console.log(data);
            $scope.allUsers = data.data;
        };

        Auth.getAllUsers(successcallbackallusers,errorcallback);

        var successcallbackAllFollowups = function(data){
            console.log(data);
            $scope.followups = data.followups;
        };

        Auth.getMyFollowUps($scope.userdetails.userid, true, successcallbackAllFollowups, errorcallback);

        $scope.getClientFollowUp = function(data){
            data.gteDate = $('.followUpOfDate').val();
            var selectedDate = new Date($('.dateofstart').val());
            selectedDate.setDate(selectedDate.getDate() + 1);

            var dd = selectedDate.getDate();
            if(dd < 10)
                dd = "0" + dd;
            var mm = selectedDate.getMonth() + 1; // 0 is January, so we must add 1
            if(mm < 10)
                mm = "0" + mm;
            var yyyy = selectedDate.getFullYear();

            data.lteDate = yyyy + "-" + mm + "-" + dd;
            console.log(data);
            var successcallbackGetMyClient = function(data){
                if(data.success){
                    $scope.followups = data.followups;
                }else{
                    $scope.showNotification("Please Select Employee!!!",data.success);
                }
            };
            Auth.getFollowUpByDateAndUserid(data, successcallbackGetMyClient, errorcallback);
        }

    }else{
        $location.path('crm/login');
    }
});
