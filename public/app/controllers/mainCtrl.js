angular.module('mainController',[])
.controller('mainCtrl',function($scope,Auth,$location,$timeout,$rootScope,AuthToken){

        $scope.userdetails = {};
        $scope.isLoggedIn = false;
        $scope.isAdmin = false;
        $scope.pageLoaded = false;
        $scope.notificationSuccessMsg = false;
        $scope.notificationFailedMsg = false;
        $scope.notificationMsg = "Notification MSG";
        $scope.gotJuniors = false;
        $scope.cities = ["agartala","agra","agumbe","ahmedabad","aizawl","ajmer","alappuzha beach","allahabad","alleppey","almora","amarnath","amritsar","anantagir","andaman and nicobar islands","araku valley","aurangabad","ayodhya","badrinath","bangalore","baroda","bastar","bhagalpur","bhilai","bhimtal","bhopal","bhubaneswar","bhuj","bidar","bilaspur","bodh gaya","calicut","chail","chamba","chandigarh","chennai","chennai beaches","cherai","cherrapunji","chidambaram","chikhaldara hills","chopta","coimbatore","coonoor","coorg","corbett national park","cotigao wild life sanctuary","cuttack","dadra and nagar haveli","dalhousie","daman and diu","darjeeling","dehradun","delhi","devikulam","dhanaulti","dharamashala","dindigul","dudhwa national park","dwaraka","faridabad","gandhinagar","gangotri","gangtok","gir wildlife sanctuary","goa","great himalayan national park","gulmarg","gurgaon","guruvayoor","guwahati","gwalior","hampi","haridwar","hogenakkal","horsley hills","hyderabad","idukki","imphal","indore","itangar","jabalpur","jaipur","jaisalmer","jalandhar","jammu","jamshedpur","jodhpur","kanchipuram","kanha national park","kanpur","kanyakumari","kargil","karwar","kausani","kedarnath","keoladeoghana national park","khajuraho","kochi","kodaikanal","kolkata","kollam","konark","kotagiri","kottakkal and ayurveda","kovalam","kovalam and ayurveda","kudremukh","kullu","kumaon","kumarakom","kumarakom and ayurveda","kumarakom bird sanctuary","kurukshetra","lakshadweep","lucknow","ludhiana","madurai","mahabalipuram","malpe beach","manas national park","mangalore","maravanthe beach","margoa","mount abu","mumbai","munnar","mussoorie","mysore","nahsik","nalanda","nanda devi national park","nandi hills","netravali wild life sanctuary","ooty","orchha","pahalgam","palakkad","panchgani","patna","patnitop","pattadakkal","periyar wildlife sanctuary","pithoragarh","pondicherry","pune","puri","pushkar","raipur","rajaji national park","rajgir","rameshwaram","ranchi","ranganthittu bird sanctuary","ranikhet","ranthambore","rishikesh","rourkela","sanchi","saputara","sariska wildlife sanctuary","shillong","shimla","sohna hills","srinagar","sunderbans","surat","tezpur","thanjavur",
            "thiruvananthapuram","thrissur","tirunelveli","tirupati","trichy","udaipur","ujjain","vaishali","valley of flowers","varanasi","varkala and ayurveda","vijayawada","vishakhapatnam","vrindhavan","warangal","wayanad","wayanad wildlife sanctuary","yercaud","zanskar"
        ];

        /* loading google charts for hierarchical display */
        google.charts.load('current', {packages:["orgchart"]});
        /* end hierarchical display */

        $scope.month = ["January", "February", "March", "April", "May", "June", "July", "August", "September" , "October", "November", "December" ];
        $scope.showNotification = function(msg,success){
            $scope.notificationMsg = msg;
            if(success){
                $scope.notificationSuccessMsg = true;
                $scope.notificationFailedMsg = false;
            } else {
                $scope.notificationSuccessMsg = false;
                $scope.notificationFailedMsg = true;
            }
            $timeout(function(){
                $scope.notificationSuccessMsg = false;
                $scope.notificationFailedMsg = false;
            },2000);
        };

        var errorcallback = function(data){
            $scope.showNotification(data.msg,data.success);
        };
        // every time when route changes it will execute this function
        $rootScope.$on('$routeChangeStart', function(){
            $scope.successMsg = false;
            $scope.errorMsg = false;

            if(Auth.isLoggedIn()){ // if token is set doesn't mean that it is a valid token which contains user details encrypted inside
                $scope.isLoggedIn = true;
                $scope.pageLoaded = true;
                fadeout();
                var successcallback = function(data){
                    //console.log(data);
                    if(data.data.juniors.length > 0){
                        $scope.gotJuniors = true;
                    } else {
                        $scope.gotJuniors = false;
                    }
                    if(data.success) { // token is set and it is valid
                        $scope.userdetails.username = data.data.username;
                        $scope.userdetails.userid = data.data.userid;
                        $scope.userdetails.email = data.data.email;
                        $scope.userdetails.name = data.data.name;
                        $scope.userdetails.gender = data.data.gender;
                        $scope.userdetails.contact = data.data.contact;
                        $scope.userdetails.mappedefforts = data.data.mappedefforts;
                        $scope.userdetails.perdaywaitage = data.data.perdaywaitage;
                        if(data.data.type == "admin"){
                            $scope.isAdmin = true;
                        }
                        $scope.isLoggedIn = true;
                        fadeout();
                    }
                    else {  // token is set but it is invalid
                        $scope.userdetails.username = "";
                        $scope.userdetails.userid = "";
                        $scope.userdetails.email = "";
                        $scope.userdetails.name = "";
                        $scope.userdetails.gender = "";
                        $scope.userdetails.contact = "";
                        $scope.userdetails.mappedefforts = [];
                        $scope.userdetails.perdaywaitage = "";
                        $scope.isLoggedIn = false;
                        AuthToken.removeToken();
                        $location.path('crm/login');
                    }
                };
                Auth.getUserFromToken(successcallback,errorcallback);
            }
            else {
                fadeout();
            }
        });

        $scope.isNumberKey = function(evt){
            //console.log(evt);
            if(isNaN(String.fromCharCode(evt.keyCode))){
                evt.preventDefault();
            }
        };
        $scope.filetypes = ['csv', 'txt', 'excel'];
        $scope.ExportTo = function (filetype,tableIDName) {
            var table = document.getElementById(tableIDName);
            //$('#clientsTable').tableExport();
            switch (filetype) {
                case 'pdf':
                    $('#'+tableIDName).tableExport({tableName: 'CRMReport',type: 'pdf', pdfFontSize: '7', escape: 'false'});
                    break;
                case 'csv':
                    $('#'+tableIDName).tableExport({tableName: 'CRMReport',type: 'csv', escape: 'false'});
                    break;
                case 'txt':
                    $('#'+tableIDName).tableExport({tableName: 'CRMReport',type: 'txt', escape: 'false'});
                    break;
                case 'excel':
                    var ignoreCol;
                    if(tableIDName == "userAllClientsTable"){
                        ignoreCol = [6];
                    }else if(tableIDName == "clientsTable"){
                        ignoreCol = [8,9];
                    }else{
                        ignoreCol = [];
                    }
                    $('#'+tableIDName).tableExport({tableName: 'CRMReport',ignoreColumn: ignoreCol,type: 'excel', escape: 'false'});
                    break;
                case 'doc':
                    $('#'+tableIDName).tableExport({tableName: 'CRMReport',type: 'doc', escape: 'false'});
                    break;
            }
        };

        // filters tasks on tabular view for both pending and completed tabs
        $scope.filterTable = function(event,index,tableIDName){
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById(event.target.id);
            filter = input.value.toUpperCase();
            table = document.getElementById(tableIDName);

            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[index];
                if (td) {
                    txtValue = td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        };
        //

        $scope.loginUser = function(data){
            $scope.successMsg = false;
            $scope.errorMsg = false;
            $scope.isAdmin = false;
            var successcallback = function(data){
                console.log(data);

                $scope.showNotification(data.msg,data.success);
                $scope.successMsg = data.msg;
                if(data.success){
                    if(data.data.juniors.length > 0){
                        $scope.gotJuniors = true;
                    } else {
                        $scope.gotJuniors = false;
                    }
                    $scope.userdetails.username = data.data.username;
                    $scope.userdetails.userid = data.data.userid;
                    $scope.userdetails.email = data.data.email;
                    $scope.userdetails.name = data.data.name;
                    $scope.userdetails.gender = data.data.gender;
                    $scope.userdetails.contact = data.data.contact;
                    $scope.userdetails.mappedefforts = data.data.mappedefforts;
                    $scope.userdetails.perdaywaitage = data.data.perdaywaitage;
                    $scope.isLoggedIn = true;
                    if(data.data.type == "admin"){
                        $scope.isAdmin = true;
                    } else {
                        $scope.isAdmin = false;
                    }
                    $timeout(function(){
                        if(data.data.type == "admin"){
                            $location.path('crm/hierarchy');
                        } else {
                            $location.path('crm/followups');
                        }
                    },500);
                }
            };
            Auth.login(data,successcallback,errorcallback);
        };

        $scope.logout = function(){
            Auth.logout();
            $scope.showNotification("Successfully Logged Out !",true);
            $scope.isLoggedIn = false;
            $location.path('crm/logout');
            $timeout(function(){
                $location.path('crm/login');
            },500);
        };
});